const { json } = require("express");
const express = require("express");
const fs =require('fs')
const path =require('path')
const app = express();
const data=fs.readFileSync(path.join(__dirname,'/data/index.json'))

const sushi=JSON.parse(data).sushi
const sweet_buns =JSON.parse(data).sweet_buns
app.get("/sushi",(req,res)=>{
    res.send(sushi)
})
app.get("/sweet buns",(req,res)=>{
    res.send(sweet_buns)
})

app.listen(3100,()=>console.log("http://localhost:3100"))