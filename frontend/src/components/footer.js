import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faInstagram,faSnapchat} from '@fortawesome/free-brands-svg-icons'
import { faEnvelope,faPhone} from "@fortawesome/free-solid-svg-icons"

import {Div,Smalldiv,Footer,Span,Parag,Bigdiv,Item,Li,Ul,H3,Socialul,FooterLi,FooterAnchore,Input,Button}from './../style/footer'
//#5d636f
//#08090b
const Myfooter = ({date}) => {
  const H3style ={
    fontFamily:"'Berkshire Swash', cursive",
    color:"white",
  fontSize:"22px"
  }
    return ( 
  <div style={{position:"relative",bottom:"0"}}>
    <Div>
<Smalldiv>

<H3> Branchs :</H3>
<Ul>
  <Li><Span >Dubai</Span><FontAwesomeIcon icon={faPhone} style={{marginRight:"10px"}}></FontAwesomeIcon>+971502877135 </Li>
  <Li><Span >Abu Dhabi</Span><FontAwesomeIcon icon={faPhone}style={{marginRight:"10px"}}></FontAwesomeIcon>+971502877135</Li>
  <Li><Span>Al Ain</Span><FontAwesomeIcon icon={faPhone}style={{marginRight:"10px"}}></FontAwesomeIcon>+971502877135</Li>
</Ul>
<Socialul>
<a><FontAwesomeIcon icon={faInstagram}style={{marginRight:"18px"}}></FontAwesomeIcon></a>
  <a ><FontAwesomeIcon icon={faSnapchat}style={{marginRight:"18px"}}></FontAwesomeIcon></a>
  <a ><FontAwesomeIcon icon={faEnvelope}style={{marginRight:"0"}}></FontAwesomeIcon></a>
</Socialul>
</Smalldiv>
<Bigdiv>
<Item>
<h2 style={{writingMode:"vertical-lr",color:"white",fontFamily:"Berkshire Swash, cursive",letterSpacing:'10px',fontSize:"28px",marginLeft:"40px",color:"#fff3bf"}}>YAMAZAKI</h2>

</Item> 
<Item>
<h3 style={H3style}>Services</h3>
<ul style={{padding:"0 20px"}}>
  <FooterLi><FooterAnchore href="#">FAQ</FooterAnchore></FooterLi>
  <FooterLi><FooterAnchore href="#">Documentation</FooterAnchore></FooterLi>
  <FooterLi><FooterAnchore href="#">Products</FooterAnchore></FooterLi>
</ul>
</Item> 
<Item>
<h3 style={H3style}>Quick Linkes</h3>
<ul style={{padding:"0 20px"}}>
  <FooterLi><FooterAnchore href="#">About Us</FooterAnchore></FooterLi>
  <FooterLi><FooterAnchore href="#">Contact Us</FooterAnchore></FooterLi>
  <FooterLi><FooterAnchore href="#">Social Media</FooterAnchore></FooterLi>
  <FooterLi><FooterAnchore href="#">Privacy Policy</FooterAnchore></FooterLi>
</ul>
</Item> 

<Item style={{position:"relative"}} >
<h3 style={H3style}>Keep in Touch </h3>
<Input type="text" placeholder="send Email"/>
<Button>Send</Button>
</Item> 
 
</Bigdiv>

    </Div>
     <Footer> 
         <Parag>
         CopyRight &copy; 2020-Yamazaki Japaness kitchen <br/>Produced By <b>
              Mostafa Dbagh</b> 
         </Parag>
       
         </Footer>
</div>
     );
}
 
export default Myfooter;

