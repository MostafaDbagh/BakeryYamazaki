import React,{useState,useEffect} from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {faQuoteLeft} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {H2section} from '../style/common'
export default function SimpleSlider() {
  const [review,setReview] = useState([])
  useEffect(()=>{
    fetch("./review.json")
    .then(res =>res.json())
    .then(data => setReview(data))
  })
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <>
    <H2section>Customer's Say About Us</H2section>
    <div style={{textAlign:"center",margin:"10px 0"}}>
        <FontAwesomeIcon icon={faQuoteLeft} style={{fontSize:"32px",marginBottom:"10px"}}/>
        </div>
    <Slider {...settings} style={{width:"80%",margin:"0 auto",position:"relative"}}>
    {review.map(item => (
      <div key={item.id} style={{position:"relative",height:"800px",width:"80%",}}>
        
     
        <h2 style={{textAlign:"center",padding:"12px"}}>{item.name}</h2>
        <p style={{width:"50%",margin:"10px auto",fontFamily:"Roboto, sans-serif",fontWeight:"700"}}>{item.review}</p>
      </div>
    ))}
   
    </Slider>
    </>
  );
}