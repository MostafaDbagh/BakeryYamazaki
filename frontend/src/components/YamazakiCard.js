import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { Minidiv,Button,Card,Image,Input,Cardbottom,H4,Cardbody,H6,Span,Parag,Togglediv } from '../style/cardstyle';

const YamazakiCard = (props) => {
    const {name,price,details,image} = props
    return ( 
       <Card >
           <div className="card-header" style={{padding:"0",marginBottom:"10px"}}>
       <Image src={image}/>
      
           </div>
           <Cardbody>
               <H4>{name} </H4>
               <Minidiv>
               <Span>{price} </Span>
               <H6>AED</H6>
               </Minidiv>
           </Cardbody>
           
           <Parag>{details}</Parag>
           
           
           <Cardbottom>
               <Input type="number"  ></Input>
               <Button>Add To Card<FontAwesomeIcon icon ={faShoppingCart} style={{"marginLeft":"10px"}}/></Button>
           </Cardbottom>
       </Card>
     );
}
 
export default YamazakiCard ;

//@styled-icons/typicons/InfoLarge