import React from 'react';

import {First,H1,Paragraph,Button,Sup,BtnContainer,Overlay,Div,H3,Textdiv,H2}from './../style/contentstyle'
const Content = () => {
    return ( 
<div style={{marginBottom:"35px"}}>
    <First>
    <Overlay>
        <Div>
        <H1>  YAMAZAKI - Bakery - Boutique</H1>
        <H2> Tasty Japanese  Savory Pastries and Sweets </H2>
     
        <BtnContainer>
        <Button>Discover Us</Button>
        <Button>Order Now</Button>
        </BtnContainer>
      
        
        </Div>
  
      

  </Overlay>

    </First>
 
 

 </div>
      
     );
}
 
export default Content;
