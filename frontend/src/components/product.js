import React, { useEffect, useState } from "react";
import {Div} from '../style/cardstyle'
import {Rdiv,Rimage,RPara} from '../style/product'

const Product = () => {
 const [randomdata,setRandom] = useState([])
  useEffect(() => {
    const fetchData = () => {
      fetch("./random.json")
        .then(res => res.json())
        .then(data => setRandom(data));
      };
      fetchData();
  }, []);
  return (
    <>
            <h2 style={{textAlign:"center",color:"#5e6f64",marginBottom:"25px",fontFamily:"'Berkshire Swash', cursive"}}>Featured Product</h2>
            <Div style={{marginBottom:"35px"}}>
{randomdata.map(i =>(
  <Rdiv key={i.id}>
 <Rimage src={i.image}/>
  <RPara>{i.name}</RPara>
  </Rdiv>
))}
</Div>
     
   
    </>
  );
};

export default Product;
