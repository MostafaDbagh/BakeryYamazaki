import React,{useState} from 'react';
import logo from '../images/logo.png'
import {Menu,Unorderlist,Listitem,Anchore, Logo,Image,Icon} from '../style/headerstyle'
import {Link} from 'react-router-dom'
import {
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Nav,
    Collapse,
    NavItem,
    NavLink
}from 'reactstrap'
import { useMediaQuery } from 'react-responsive'

const Header = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })
    let style_link={}
    if(isTabletOrMobile)
        style_link = {
            color: "white",
            fontSize: "20px",
            fontWeight: "bold",
            marginLeft:"62px"
        }
    else  style_link = {
        color: "white",
        fontSize: "16px",
        fontWeight: "bold"
    }
    const toggle = () => setIsOpen(!isOpen);
    return ( 
    <Navbar color="dark" dark  expand="md"style={{height:"80px"}} >
    {/* <Menu> */}
        <NavbarBrand>
            <Logo>
                <Link exact to='/'>
                    <Image src={logo}></Image>
                </Link>
            </Logo>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
        <Nav className="mx-auto" navbar>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{ textDecoration: "none", }} to='/sushi'>
                    <NavLink style={style_link}>SUSHI</NavLink>
                </Link>
            </NavItem>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{textDecoration:"none"}}  to='/sweets_buns'>
                    <NavLink style={style_link}>
                        SWEETS BUNS
                    </NavLink>
                </Link>
            </NavItem>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{textDecoration:"none"}} to='/soup'>
                    <NavLink style={style_link}>
                        SOUP
                    </NavLink>
                </Link>
            </NavItem>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{ textDecoration: "none" }} to='/cakes'>
                    <NavLink style={style_link}>
                        CAKES
                    </NavLink>
                </Link>
            </NavItem>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{textDecoration:"none"}}  to='/cold_beverages'>
                    <NavLink style={style_link}>
                        COLD BEVERAGES  
                    </NavLink>
                </Link>
            </NavItem>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{textDecoration:"none"}} to='/hot_beverages'>
                    <NavLink style={style_link} >
                        HOT BEVERAGES
                    </NavLink>
            </Link>
            </NavItem>
            <NavItem style={{marginRight:"20px"}}>
                <Link style={{textDecoration:"none"}} to='/other'>
                    <NavLink style={style_link}> 
                        OTHER
                    </NavLink>
            </Link>
            </NavItem>
        </Nav>
        </Collapse>
    {/* </Menu> */}
        </Navbar>
            );
}
 
export default Header;