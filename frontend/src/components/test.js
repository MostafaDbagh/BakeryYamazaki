

import React from 'react'
import styled, { keyframes } from 'styled-components'
import university from  '../style/images/uniersity.jpg'
import century from  '../style/images/century.jpg'
import {H2section} from '../style/common.js'
import Abudhabi from  '../style/images/Abudhabi.jpg'
const Breathe = () => {
 return (
     <>
      <H2section>Where To Find Us</H2section>
  <Container>
   <Animationdiv  >
   <ImageDiv src={Abudhabi}/>
<Para> Abudhabi-Defense Road</Para>
     
       </Animationdiv>
   <Animationdiv   >
      <ImageDiv src={century}/>
      <Para>
          Dubai Century Mall
      </Para>
       </Animationdiv>
   <Animationdiv   >
   <ImageDiv src={century}/>
   <Para>Alain</Para>
       </Animationdiv>
   <Animationdiv   >
   <ImageDiv src={century}/>
   <Para>Abudhabi-University</Para>
       </Animationdiv>

  </Container>
  </>
 )
}
export default Breathe
const leftAnimation = keyframes`
 0% { left:-400px; }
 
 100% { left:0px; }
`
const rightAnimation = keyframes`
 0% { right:-400px; }
 
 100% { right:0px; }
`
const Para = styled.p`
font-family:"Berkshire Swash, cursive";
color:white;
position:absolute;
top:50%;
left:50%;
opacity:0;
transition:1s ease opacity;

`
const Animationdiv = styled.div`
 
 

height:100%;
over-flow:hidden;
 position:relative;

 left:
 &:nth-child(odd){
     animation-delay:.7s
 }
 &:nth-child(even){
     animation-delay:.4s
 }
&:before{
  content:'';
  position:absolute;
  top:0;
  left:0;
  width:0;
  height:100%;
  background:rgba(0,0,0,0.9);
  color:white;
  transition: 1s ease all;
    
}
&:hover:before{
width:100%;
}
&:hover p{
    
opacity:0.8;

}
`
const ImageDiv =styled.img`
width:100%;
height:275px;
&:hover{
  
}


`
const Container = styled.div`

width:100%;
position:relative;


padding:35px 0;
display:grid;
grid-template-columns:repeat(1,2fr 1fr);
grid-gap:5px;
over-flow:hidden;

animation:safi 2s;
&:hover{
    margin:0 auto;
    animation-name:${leftAnimation};
 animation-duration: .8s;
 animation-iteration-count: 1; 
 animation-timing-function: ease;
 animation-fill-mode: forwards;
}
@media(max-width:450px){
    grid-template-columns:repeat(1,1fr);
}

`