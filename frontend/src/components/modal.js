import React from "react";
import { Modaldiv, Modaltitle, Para } from "./../style/modalstyle";
const Modal = props => {
  const { tomato, melon, apple, banana, orange, pine, getIsclick } = props;
  return (
    <Modaldiv>
      <Modaltitle>All Sales</Modaltitle>
      <div>
        {tomato ? (
          <Para>
            you want <b>{tomato}</b> Kg Tomato
          </Para>
        ) : (
          <p />
        )}
        {melon ? (
          <Para>
            you want <b>{melon}</b> Kg Melon
          </Para>
        ) : (
          <p />
        )}
        {apple ? (
          <Para>
            you want <b>{apple}</b> Kg Apple
          </Para>
        ) : (
          <p />
        )}
        {banana ? (
          <Para>
            you want <b>{banana}</b> Kg Banana
          </Para>
        ) : (
          <p />
        )}
        {orange ? (
          <Para>
            you want <b>{orange}</b> Kg Orange
          </Para>
        ) : (
          <p />
        )}
        {pine ? (
          <Para>
            you want <b>{pine}</b> Kg Pine{" "}
          </Para>
        ) : (
          <p />
        )}
      </div>
      <button onClick={() => getIsclick()}>Hide</button>
    </Modaldiv>
  );
};

export default Modal;
