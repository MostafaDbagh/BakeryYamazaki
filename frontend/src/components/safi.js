import React,{useEffect,useState} from 'react';
import { Div } from '../style/cardstyle';
import Yamazakicard  from './YamazakiCard'
const Safi = () => {
    const [mydata,setMyData] = useState([])
    useEffect(() => {
        const fetchData = () => {
          fetch("./data.json")
            .then(res => res.json())
            .then(data => setMyData(data));
         
        };
        fetchData();
      }, []);
    return ( 
        <>
        <h2 style={{textAlign:"center",color:"#5e6f64",marginBottom:"25px"}}>Featured Product</h2>
        <Div>
{mydata.map(item => 
<Yamazakicard
key={item.id}
image={item.image}
name={item.name}
price={item.price}
details= {item.details}
/>
 )}
 </Div>
        </>
     );
}
 
export default Safi;