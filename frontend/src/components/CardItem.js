import React,{useState,useRef} from 'react';
import {Card,CardUp,Image,Parag,Button} from './../style/cardstyle'

const Productcard = ({name,price,img,num,clicknum,getTomato,getMelon,getApple,getBanana,getOrange,getPine}) => {
        const saleRef = useRef();
        const priceRef = useRef();
        let [nums,setNums] = useState(0);

     const CollectItems = ()=>{
             setNums(c=>+c+1);
             if(num===1)getTomato(nums+1);
             else if(num===2)getMelon(nums+1)
             else if(num===3)getApple(nums+1)
             else if(num===4)getBanana(nums+1)
             else if(num===5)getOrange(nums+1)
             else if(num===6)getPine(nums+1)  
}
    return(  
       <Card>
       <CardUp>
 <Image src={img} alt="" />
 </CardUp>
  <div className="card-bottom">
<Parag ref={saleRef}> Product_name:{name}</Parag>
<Parag ref={priceRef}>Price:{price}</Parag>
        </div>
        <Button onClick={()=>CollectItems()}>Add To Card<i className="fa fa-shopping-cart fa-2x"></i></Button> 
        </Card>
     );
}
 
export default Productcard;


