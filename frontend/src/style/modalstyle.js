import styled from "styled-components";

const Modaldiv = styled.div`
  width: 40vw;
  margin: 0 auto;
  border: 1px solid #5e6f64;
`;
const Modaltitle = styled.h5`
  font-size: 24px;
  padding: 16px;
  color: #212529;
  border-bottom: 1px solid rgba(0, 0, 0, 0.3);
  margin: 0;
  letter-spacing: 1px;
`;
const Para = styled.p`
  font-size: 26px;
  padding: 16px;
  margin: 0;
  border-bottom: 1px solid #5e6f64;
  text-align: center;
`;

export { Modaldiv, Modaltitle, Para };
<Menu>
<Unorderlist>
    <Listitem><Anchore href="#">SUSHI  </Anchore></Listitem>
    <Listitem><Anchore href="#">SWEETS BUNS</Anchore></Listitem>
    <Listitem><Anchore href="#">SOUP</Anchore></Listitem>
    <Listitem><Anchore href="#">CAKES</Anchore></Listitem>

</Unorderlist>
<Logo>
    <Image src={logo}></Image>
</Logo>
<Unorderlist>
    <Listitem><Anchore href="#">COLD BEVERAGES</Anchore></Listitem>
    <Listitem><Anchore href="#">HOT BEVERAGES</Anchore></Listitem>
    <Listitem><Anchore href="#">OTHERS</Anchore></Listitem>

</Unorderlist>
        </Menu>
