import styled ,{keyframes } from 'styled-components'


const Div = styled.div`

width:94%;
margin:0 auto;


background:black;
`

const Animationdiv= styled.div`


background:purple;
width:200px;
height:150px;
font-size: 1.2rem;
text-align:center;


`
const Scale = keyframes `
0% { height: 100px; width: 100px; }
30% { height: 400px; width: 400px; opacity: 1 }
40% { height: 405px; width: 405px; opacity: 0.3; }
100% { height: 100px; width: 100px; opacity: 0.6; }
`;
export {Div,Animationdiv}

// animation: ${Scale} 2s linear infinite;