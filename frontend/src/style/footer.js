import styled from 'styled-components'


const Div = styled.div`
width:100%;

background:black;
position:relative;
`
const Input = styled.input`
width:100%;
height:45px;
border-radius:5px;
margin-bottom:12px;

`
const Button = styled.button`
position:absolute;
width: 95px;
    height: 37px;
    outline: none;
    padding: 7px 19px;
    border: none;
    background: #5d636f;
    color: white;
    border-radius: 10px;
    top:100px;
    right:0;
    font-family: 'Roboto', sans-serif;
    font-weight:bold;
    @media(max-width:500px){
        width:70px;
       

    }`
const Smalldiv = styled.div`
width:285px;
padding:30px;
height:250px;
background:#5d636f;
position:absolute;
top:-45px;
left:100px;
border-radius:2%;
@media(max-width:500px){
    left:70px;
    height:235px;
    top:-165px
  

}
`
const Bigdiv = styled.div`
display:grid;
grid-template-columns:repeat(4,1fr);
justify-content:space-between;
width:90%;
justify-Items:space-around;

@media(max-width:450px){
    height:auto;
    grid-template-columns:repeat(2,1fr); 
}

`

const Footer = styled.footer`
width:100%;
font-size:14px;
height:70px;
background:#5d636f;
color:white;
text-align:center;
position:relative;
`
const Span = styled.span`
width:80px;
display:inline-block;
text-align:center;

`
const Parag = styled.p`
width:100%;
text-align:center;
letter-spacing:1px;
margin:0;
position:absolute;
top:50%;
left:50%;
transform:translate(-50%,-50%)
`

const H3 = styled.h3`
font-size:20px;
color:white;

text-align:center;
font-weight:bold;
@media(max-width:500px){
    font-size:16px;
}

`
const Item = styled.div`
margin-top:150px;
text-align:center;
@media(max-width:500px){
    margin-top:70px; 
}
`
const Ul =styled.ul`
margin:0;
padding:0
`
const Socialul = styled.div`
padding:10px;
text-align:center;
color:white;
`
const Li = styled.li`
width:100%;
text-align:start;
list-style:none;
padding:7px;
color:white;
font-size:14px;
font-weight:bold;

`
const FooterLi = styled.li`
list-style:none;
margin:8px;

`
const FooterAnchore = styled.a`
text-decoration:none;
color:white;
font-family: 'Roboto', sans-serif;
font-weight:bold;
`

export {Div,Smalldiv,Footer,Span,Parag,Bigdiv,Item,Li,Ul,H3,Socialul,FooterLi,FooterAnchore,Input,Button}