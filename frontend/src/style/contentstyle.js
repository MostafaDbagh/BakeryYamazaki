import styled from "styled-components";
import shopping from "./../images/1.jpg";


const SmallDiv = styled.div`
display:flex;
justify-content:center;
align-items:center;
`
const H2 = styled.h2`
text-align:center;
@media(max-width:500px){
  font-size:18px;
  
}
`
const Overlay = styled.div`
width:100%;
height:100%;
background:rgba(0,0,0,0.6);
display: flex;
flex-direction: row;
justify-content:space-between;
align-items:space-between;
`
const H1 = styled.h1`
 color:#ffffff;
 letter-spacing:2px;
 font-size:34px;
 font-weight:600;
 margin: 0 0 8px;
 color:#fff3bf;
 font-style:italic;
 font-family: 'Berkshire Swash', cursive;

 @media(max-width:500px){
   font-size:24px;
   letter-spacing:1px;
 }
`;

const Div = styled.div`
width:100%;
color:white;
display:flex;
flex-direction:column;
align-items:center;
margin-top:150px

`

const First = styled.div`
  width: 100%;
  height:560px;
  background-image: url(${shopping});
  clip-path: polygon(0 0, 100% 0%, 100% 68%, 0% 100%);
  background-repeat:no-repeat;
  background-size:cover;
  background-Position:  30% 30%;
  position:relative;
`;


const Paragraph = styled.p`

  font-size: 2rem;
  font-weight: bold;
  color: #5e6f64;

 
`;
const Button = styled.button`
  width: 130px;
  height: 40px;
  background:#5d636f ;
  border: none;
  color: white;
  font-size: 16px;
  font-weight: 600;
  padding: 7px;
  text-align: center;
  margin-right: 0.5rem;
  margin: 10px;
`;
const H3 = styled.h3`
color:white;
font-size:30px;
width:100%;
text-align:center;
`
const BtnContainer  = styled.div`
display:flex;
flex-direction:row;

`

export { First, H1, Paragraph, Button,Overlay,Div,SmallDiv,H3,BtnContainer,H2};
