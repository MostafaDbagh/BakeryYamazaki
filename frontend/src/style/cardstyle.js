import styled from "styled-components";

const Div = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  width: 90%;
  margin: auto;
  position: relative;
`;
const Minidiv = styled.div`
display:flex;
flex-direction:column;
justify-content:center;

`
const Span = styled.span`
width:40px;
height:40px;
background:#ffd369;
color:white;
font-weight:bold;
line-height:40px;
text-align:center;

`
const Input = styled.input`
width:50px;
height:43px;
padding:0;
font-size:1.6rem;
margin-left:24px;


text-align:center;
`
const Cardbottom = styled.div`
display:flex;
justify-content:space-between;
position:absolute;
bottom:20px
`
const Modaldiv = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
`;

const Card = styled.div`
  width: 300px;
  height: 340px;
  margin: 16px;
  word-wrap:break-word;
  over-flow:hidden;
  position: relative;
  box-shadow:-2px -0px 3px 0px #08090b; 
  position:relative;
  &:hover{
    color:red
  }


 

`;
const CardUp = styled.div`
  position: absoult;
  width: 100%;
  height: 50%;
`;
const Image = styled.img`
  
  width: 300px;
  height: 180px;

  `

const Parag = styled.p`
  font-size: 16px;
  font-weight: bold;
  color: #5e6f64;
  margin:12px 0;
  text-align:center;

`;
const Button = styled.button`
  width: 150px;
  height: 43px;
  padding: 8px;
  background: #222831;
  color: white;
  border: none;
  font-size: 1rem;
  font-weight: bold;
  cursor: pointer;
  margin-left:38px;
`;
const H4 = styled.h4`


  font-size: 1.5rem;
  color: #5e6f64;
  position: relative;
  text-transform: capitlize;
  
  text-align: center;
  margin:0 8px;

`
const Cardbody = styled.div`
display:flex;
justify-content:space-around;
align-items:center;

`
const MYButton = styled.button`
  width: 150px;
  height: 150px;
  border-radius: 50%;
  background: #100cbe;
  color: white;
  border: none;
  font-size: 1.3rem;
  font-weight: bold;
  cursor: pointer;
  text-align: center;
  margin: 0 auto;
  opacity: 0.49;
`;
const Vdiv = styled.div`
  display: flex;
  justify-content: center;
`;
const H6 = styled.h6`
margin:0 3px;
color:#5e6f64;
font-size:16px;
font-weight:italic;
letter-spacing:1px
`

export {
  Div,
  H6,
  Card,
  CardUp,
  Cardbottom,
  Image,
  Parag,
  Button,
  H4,
  Minidiv,
  MYButton,
  Modaldiv,
  Vdiv,
  Input,
  Cardbody,
  Span,
};
