import styled from "styled-components"


const H2section = styled.h2`
text-align:center;
font-family:Berkshire Swash, cursive;
margin:35px 0;
position:relative;
&:before{
content:'';
position:absolute;
width:0;
height:2px;
background:blue;
bottom:-5px;
left:45%;


transition:.8s ease all;
}
&:hover:before{
width:10%;
}
`

export {H2section}